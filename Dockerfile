FROM ubuntu:16.04

USER root
RUN DEBIAN_FRONTEND=noninteractive && apt-get update && apt-get install -y \
  apt-utils \
  curl \
  software-properties-common
# node
WORKDIR /tmp
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y \
  nodejs
RUN mkdir -p /var/www/app
COPY ./app/package.json /var/www/app/
WORKDIR /var/www/app
RUN npm install
RUN npm install pm2 -g

