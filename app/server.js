'use strict';

const Hapi = require('hapi');
const server = new Hapi.Server({ });
const Routes = require('./routes');
const mongojs = require('mongojs');

server.connection({ host: '0.0.0.0', port: 3000 });
server.register([], (err) => {
  if (err) {
    throw err;
  }
  server.route(Routes.endpoints); 
  server.start((err) => {
    if (err) {
      throw err;
    }
    console.log('Server running at:', server.info.uri);
    // db connection. sets index for searches
    server.app.db = mongojs('mongodb/tikal', ['agents']);
    server.app.db.agents.createIndex({"location":"2dsphere"});
  });
});

