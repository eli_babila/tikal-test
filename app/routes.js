const Joi = require('joi');
const Controllers = require('./controllers');

exports.endpoints = [
  {
    method: 'GET',
    path:'/countries-by-isolation',
    config: {
      auth: false,
    },
    handler: Controllers.cbyi, 
  },
  {
    method: 'POST',
    path:'/find-closest',
    config: {
      auth: false,
      validate: { 
        payload: { 
          'targetlocation': Joi.string().required()
        } 
      },
    },
    handler: Controllers.find_closest, 
  },
];


