'use strict';

const fetch = require('node-fetch');

exports.cbyi = function (request, reply) {
  request.server.app.db.agents.mapReduce( 
    function() { emit (this.agent, {country: this.country,  amount: 1}) }, 
    function (k, v) { return { country:v[0].country,  amount: v.length}; }, 
    { out:  "agents_total" }, (err, doc) => {
      doc.mapReduce(
        function() { emit(this.value.country, 1) }, 
        function (k,v) { return Array.sum(v) }, 
        { query: { "value.amount": 1 },  out: "agent_results"  }, (err,doc2) => {
          //doc2.find().toArray((e, a) => { console.log('=>' + JSON.stringify(a)); });
          doc2.aggregate({ $group : { _id: null, max: { $max : "$value" }}}, (err, doc3) => {
            // bypassing mongo's docker limitation
            doc2.find({ value: doc3[0].max }).toArray((err, doc4)=>{
              var res = [];
              for(let x=0;x<doc4.length;x++) {
                res.push(doc4[x]._id);
              }
              return reply({ 'country' : res, 'value': doc4[0].value });
            });
          });
        }
      );
    }
  );
};


exports.find_closest = function(request, reply) {
  // OSM Nominatim service
  var geocode = 'http://nominatim.openstreetmap.org/search?format=json&q=' + request.payload.targetlocation;
  fetch(geocode)
	.then(res => res.json())
	.then(function(data) {
    //console.log('=>'+data[0].lat, data[0].lon);
    request.server.app.db.agents.aggregate( [ { "$geoNear":{ "near": { type: "Point", coordinates: [ parseFloat(data[0].lat), parseFloat(data[0].lon) ] }, "distanceField": "dist.calculated", "includeLocs": "dist.location", "distanceMultiplier":1/1000, "spherical": true } } ] ).toArray((e, a) => {
      let fdx = a.length - 1;
      return reply({ 'near' : { agent: a[0].agent, country: a[0].country, address: a[0].address, date: a[0].date   },
                     'far' : { agent: a[fdx].agent, country: a[fdx].country, address: a[fdx].address, date: a[fdx].date   } 
        });
    });
  })
  .catch(err => { console.log(err); reply({error: JSON.stringify(err)})  });
}

